import pandas as pd
import numpy as np

from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import TruncatedSVD
from sklearn.preprocessing import StandardScaler
from sklearn import decomposition, pipeline, metrics, grid_search
from sklearn.base import BaseEstimator

from nltk.stem.porter import *

import re
import math

from main import *

import scipy.sparse
    
from bs4 import BeautifulSoup
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC
from sklearn.feature_extraction import text
from sklearn.metrics.pairwise import linear_kernel

import string

from sklearn.pipeline import Pipeline, FeatureUnion

# The following 3 functions have been taken from Ben Hamner's github repository
# https://github.com/benhamner/Metrics
def confusion_matrix(rater_a, rater_b, min_rating=None, max_rating=None):
    """
    Returns the confusion matrix between rater's ratings
    """
    assert(len(rater_a) == len(rater_b))
    if min_rating is None:
        min_rating = min(rater_a + rater_b)
    if max_rating is None:
        max_rating = max(rater_a + rater_b)
    num_ratings = int(max_rating - min_rating + 1)
    conf_mat = [[0 for i in range(num_ratings)]
                for j in range(num_ratings)]
    for a, b in zip(rater_a, rater_b):
        conf_mat[a - min_rating][b - min_rating] += 1
    return conf_mat


def histogram(ratings, min_rating=None, max_rating=None):
    """
    Returns the counts of each type of rating that a rater made
    """
    if min_rating is None:
        min_rating = min(ratings)
    if max_rating is None:
        max_rating = max(ratings)
    num_ratings = int(max_rating - min_rating + 1)
    hist_ratings = [0 for x in range(num_ratings)]
    for r in ratings:
        hist_ratings[r - min_rating] += 1
    return hist_ratings


def quadratic_weighted_kappa(y, y_pred):
    """
    Calculates the quadratic weighted kappa
    axquadratic_weighted_kappa calculates the quadratic weighted kappa
    value, which is a measure of inter-rater agreement between two raters
    that provide discrete numeric ratings.  Potential values range from -1
    (representing complete disagreement) to 1 (representing complete
    agreement).  A kappa value of 0 is expected if all agreement is due to
    chance.
    quadratic_weighted_kappa(rater_a, rater_b), where rater_a and rater_b
    each correspond to a list of integer ratings.  These lists must have the
    same length.
    The ratings should be integers, and it is assumed that they contain
    the complete range of possible ratings.
    quadratic_weighted_kappa(X, min_rating, max_rating), where min_rating
    is the minimum possible rating, and max_rating is the maximum possible
    rating
    """
    rater_a = y
    rater_b = y_pred
    min_rating=None
    max_rating=None
    rater_a = np.array(rater_a, dtype=int)
    rater_b = np.array(rater_b, dtype=int)
    assert(len(rater_a) == len(rater_b))
    if min_rating is None:
        min_rating = min(min(rater_a), min(rater_b))
    if max_rating is None:
        max_rating = max(max(rater_a), max(rater_b))
    conf_mat = confusion_matrix(rater_a, rater_b,
                                min_rating, max_rating)
    num_ratings = len(conf_mat)
    num_scored_items = float(len(rater_a))

    hist_rater_a = histogram(rater_a, min_rating, max_rating)
    hist_rater_b = histogram(rater_b, min_rating, max_rating)

    numerator = 0.0
    denominator = 0.0

    for i in range(num_ratings):
        for j in range(num_ratings):
            expected_count = (hist_rater_a[i] * hist_rater_b[j]
                              / num_scored_items)
            d = pow(i - j, 2.0) / pow(num_ratings - 1, 2.0)
            numerator += d * conf_mat[i][j] / num_scored_items
            denominator += d * expected_count / num_scored_items

    return (1.0 - numerator / denominator)

class FeatureInserter(BaseEstimator):  
    def __init__(self):
        pass
        
    def transform(self, X, y=None):
        distances = []
        quasi_jaccard = []
            
        for row in X.tocsr():
            row = row.toarray().ravel()
            cos_distance = linear_kernel(row[:row.shape[0]/2], row[row.shape[0]/2:])
            distances.append(cos_distance[0])
            intersect = row[:row.shape[0]/2].dot(row[row.shape[0]/2:])
            union = (row[:row.shape[0]/2]+row[row.shape[0]/2:]).dot((row[:row.shape[0]/2]+row[row.shape[0]/2:]))
            quasi_jaccard.append(1.0*intersect/union)
            
        return np.matrix([x for x in zip(distances, quasi_jaccard)])
            
    def fit(self, X,y):
        return self    
        
    def fit_transform(self, X, y, **fit_params):
        self.fit(X,y)
        return self.transform(X)

if __name__ == '__main__':

    train = pd.read_csv("../Data/train.csv")
    test  = pd.read_csv("../Data/test.csv")
    idx = test.id.values.astype(int)
    y = train.median_relevance.values
    
    prep = WordVecApp()
    
    _train = prep.tf_features_query_title(train)
    _test  = prep.tf_features_query_title(test)

    
    vectorizer = TfidfVectorizer(min_df=3,  max_features=None,   
                strip_accents='unicode', analyzer='word', token_pattern=r'\w{1,}',
                ngram_range=(1, 3), use_idf=1,smooth_idf=1,sublinear_tf=1)
    

    vectorizer.fit(_train)
    X_train = vectorizer.transform(_train) 
    X_test  = vectorizer.transform(_test)

    svd = TruncatedSVD()
    scl = StandardScaler()
    svm_model = SVC(kernel='rbf')
    
    clf = pipeline.Pipeline([('un', FeatureUnion([('svd', svd), ('dense_features', FeatureInserter())])),
                             ('scl', scl),
                             ('svm', svm_model)])

    param_grid = {'un__svd__n_components' : [200, 300, 400],
                  'svm__C': [8, 10, 12]}

    kappa_scorer = metrics.make_scorer(quadratic_weighted_kappa, greater_is_better = True)
    model = grid_search.GridSearchCV(estimator = clf, param_grid=param_grid, scoring=kappa_scorer,
                                     verbose=10, n_jobs=1, iid=True, refit=True, cv=4)

    model.fit(X_train, y)

    print(model.best)

    preds = model.predict(X_test)

    submission = pd.DataFrame({"id": idx, "prediction": preds})
    submission.to_csv("tfidfinit.csv", index=False)
