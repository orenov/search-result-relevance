from main import *

import pandas as pd

if __name__ == '__main__':

    print("Load the training file")
    train = pd.read_csv('../Data/train.csv')
    test = pd.read_csv('../Data/test.csv')
    
    idx = test.id.values.astype(int)
    train = train.drop('id', axis=1)
    test = test.drop('id', axis=1)
    
    y = train.median_relevance.values
    train = train.drop(['median_relevance', 'relevance_variance'], axis=1)
    
    clf = WordVecApp()
    clf.load_google()
    clf.compute_features(test, "countMulSim", "../Features/testsimilaritygoogle.csv")
    clf.compute_features(train, "countMulSim", "../Features/trainsimilaritygoogle.csv")
    
    sys.exit(1)