import pandas as pd
import numpy as np

from gensim.models import Word2Vec
from nltk.corpus import stopwords
from nltk.stem import *

import nltk

import sys
import re
import pickle
import os.path
import math
import scipy

from bs4 import BeautifulSoup
from pyquery import PyQuery as pq

from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from sklearn.grid_search import GridSearchCV
from sklearn.base import BaseEstimator
from sklearn.decomposition import TruncatedSVD
from sklearn.preprocessing import StandardScaler
from sklearn.feature_extraction import text
from sklearn import decomposition, pipeline, metrics, grid_search

from collections import Counter
from collections import OrderedDict

# The following 3 functions have been taken from Ben Hamner's github repository
# https://github.com/benhamner/Metrics
def confusion_matrix(rater_a, rater_b, min_rating=None, max_rating=None):
    """
    Returns the confusion matrix between rater's ratings
    """
    assert(len(rater_a) == len(rater_b))
    if min_rating is None:
        min_rating = min(rater_a + rater_b)
    if max_rating is None:
        max_rating = max(rater_a + rater_b)
    num_ratings = int(max_rating - min_rating + 1)
    conf_mat = [[0 for i in range(num_ratings)]
                for j in range(num_ratings)]
    for a, b in zip(rater_a, rater_b):
        conf_mat[a - min_rating][b - min_rating] += 1
    return conf_mat


def histogram(ratings, min_rating=None, max_rating=None):
    """
    Returns the counts of each type of rating that a rater made
    """
    if min_rating is None:
        min_rating = min(ratings)
    if max_rating is None:
        max_rating = max(ratings)
    num_ratings = int(max_rating - min_rating + 1)
    hist_ratings = [0 for x in range(num_ratings)]
    for r in ratings:
        hist_ratings[r - min_rating] += 1
    return hist_ratings


def quadratic_weighted_kappa(y, y_pred):
    """
    Calculates the quadratic weighted kappa
    axquadratic_weighted_kappa calculates the quadratic weighted kappa
    value, which is a measure of inter-rater agreement between two raters
    that provide discrete numeric ratings.  Potential values range from -1
    (representing complete disagreement) to 1 (representing complete
    agreement).  A kappa value of 0 is expected if all agreement is due to
    chance.
    quadratic_weighted_kappa(rater_a, rater_b), where rater_a and rater_b
    each correspond to a list of integer ratings.  These lists must have the
    same length.
    The ratings should be integers, and it is assumed that they contain
    the complete range of possible ratings.
    quadratic_weighted_kappa(X, min_rating, max_rating), where min_rating
    is the minimum possible rating, and max_rating is the maximum possible
    rating
    """
    rater_a = y
    rater_b = y_pred
    min_rating=None
    max_rating=None
    rater_a = np.array(rater_a, dtype=int)
    rater_b = np.array(rater_b, dtype=int)
    assert(len(rater_a) == len(rater_b))
    if min_rating is None:
        min_rating = min(min(rater_a), min(rater_b))
    if max_rating is None:
        max_rating = max(max(rater_a), max(rater_b))
    conf_mat = confusion_matrix(rater_a, rater_b,
                                min_rating, max_rating)
    num_ratings = len(conf_mat)
    num_scored_items = float(len(rater_a))

    hist_rater_a = histogram(rater_a, min_rating, max_rating)
    hist_rater_b = histogram(rater_b, min_rating, max_rating)

    numerator = 0.0
    denominator = 0.0

    for i in range(num_ratings):
        for j in range(num_ratings):
            expected_count = (hist_rater_a[i] * hist_rater_b[j]
                              / num_scored_items)
            d = pow(i - j, 2.0) / pow(num_ratings - 1, 2.0)
            numerator += d * conf_mat[i][j] / num_scored_items
            denominator += d * expected_count / num_scored_items

    return (1.0 - numerator / denominator)


class WordVecApp(BaseEstimator):

    def __init__(self):
        self.min_word_count = 15
        
        stop_words = ['http','www','img','border','color','style','padding','table','font','thi','inch','ha','width','height']
        self.stop_words = text.ENGLISH_STOP_WORDS.union(stop_words)
        
        
    def __filter_words(self, words):
        ww = []
        for w in words:
            if w == "shoppe":
                w = "shop"
            elif w == "prom":
                w = "evening"
            elif w == "assassinss":
                w = "assassins"
            elif w == "lc":
                w = "conrad"
            elif w == "videogames":
                w = "videogame"
            elif w == "fragance": # add more good synonyms adidas=>sport
                w = "fragrance"
            elif w == "refrigirator":
                w = "refrigerator" 
            elif w == "legos":
                w = "lego"
            elif w == "extenal":
                w = "external"
            elif w == "hardisk": # add expressions like "hard drive"
                w = "drive"
            elif w == "victorias":
                w = "victoria"  #bad
            elif w == "tupperware":
                w = "container"
            elif w == "anime":
                w = "animal"
            elif w == "fryers":
                w = "fryer"
            elif w == "separators":
                w = "spacer"
            elif w == "turntables":
                w = "turntable"
            elif w == "squared":
                w = "square"
            elif w == "rechargable":
                w = "rechargeable"
            elif w == "longboard":
                w = "board" #bad
            elif w == "hallmark":
                continue
            elif w == "dollhouse":
                w = "house"
            elif w == "bathtub":
                w == ""
            ww.append(w)

        stemmer = PorterStemmer()
        ww = [stemmer.stem(word) for word in ww]

        return ww

    def __sentence_to_wordlist(self, sentences, remove_stopwords=False):
        sentences = re.sub("[^a-zA-Z0-9]"," ", sentences)
        words = sentences.lower().split()
        
        if remove_stopwords:
            words = [w for w in words if not w in self.stop_words]
            words = self.__filter_words(words)
        return words

    def __list_to_sentences(self, list_of_text, tokenizer, remove_stopwords=False):
        list_of_text = re.sub("<table>(.*?)</table>", " ", list_of_text)
        list_of_text = re.sub("[^a-zA-Z0-9\\s]","", list_of_text)
        
        if len(list_of_text) == 0:
            return [""]
        raw_sentences = tokenizer.tokenize(list_of_text.strip())
        sentences = []
        for raw_sentence in raw_sentences:
            if len(raw_sentence) > 0:
                sentences.append(self.__sentence_to_wordlist(raw_sentence, remove_stopwords))
            elif len(raw_sentence) == 0 :
                sentences.appned("")
         
        return sentences


    def __get_wordlist(self, list_of_text):
        output = []
        #nltk.download() #to download punkt & stopwords
        tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')

        for text in list_of_text:
            output += self.__list_to_sentences(text, tokenizer, True)
    
        return output

    def __train_wordvec(self, data):
        num_features = 100    # Word vector dimensionality                      
        self.min_word_count = self.min_word_count   # Minimum word count    
        self.num_features = num_features                    
        num_workers = 2       # Number of threads to run in parallel
        context = 8           # Context window size                                                                                    
        downsampling = 1e-3   # Downsample setting for frequent words

        print "Training model..."

        model = Word2Vec(data, workers=num_workers, \
                size = num_features, min_count = self.min_word_count, \
                window = context, sample = downsampling)

        model.init_sims(replace=True)
        model.save("wvmodel")

        return model

    def create_corpus(self, traindata, testdata):
        #Try add query
        traindata = list(traindata.apply(lambda x:'%s %s %s' % (pq(x['query']).text()+".",\
                                     pq(x['product_title']).text(), pq(x['product_description']).text()),axis=1))
        testdata  = list(testdata.apply(lambda x:'%s %s %s'  % (pq(x['query']).text()+".", \
                                     pq(x['product_title']).text(), pq(x['product_description']).text()),axis=1))

        spl_traindata = self.__get_wordlist(traindata)
        spl_testdata  = self.__get_wordlist(testdata)
        
        self.spl_total = spl_traindata + spl_testdata

        print("Computing Dict...")
        if os.path.isfile("../Utils/dict.p"):
            self.cc = pickle.load( open( "../Utils/dict.p", "rb" ) )
            self.cc = dict((k, v) for k, v in self.cc.items() if v > self.min_word_count)
        else:
            ll = sum(self.spl_total, [])
            self.cc  = dict(Counter(ll))
            self.cc = dict((k, v) for k, v in self.cc.items() if v > self.min_word_count)
            pickle.dump(self.cc, open("../Utils/dict.p", "wb"))
        print("Finish Loading Dict...")

        self.model = self.__train_wordvec(self.spl_total)


    def load_google(self):
        self.model = Word2Vec.load_word2vec_format("../Utils/GoogleNews-vectors-negative300.bin", binary = True)


    def __compute_vector(self, words):
        featureVec = np.zeros((self.num_features,),dtype="float32")
        nwords = 0.
        index2word_set = set(self.model.index2word)
        for word in words:
            if word in index2word_set: 
                nwords = nwords + 1.
                featureVec = np.add(featureVec,self.model[word])
            
        featureVec = np.divide(featureVec,nwords)

        return featureVec

    def pure_features_sum(self, X_train, filename):
        data = list(X_train.apply(lambda x:'%s %s %s' % (pq(x['query']).text()+".",\
                                     pq(x['product_title']).text(), pq(x['product_description']).text()),axis=1))
        spl_data = self.__get_wordlist(data)
        features_vec = np.zeros((len(spl_data),self.num_features),dtype="float32")
        
        for i in range(len(spl_data)):
            features_vec[i] = self.__compute_vector(spl_data[i])

        df = pd.DataFrame(features_vec)
        df.to_csv(filename, index = False) 
        

    def pure_features_subtr(self, X_train, filename):
        query_data = list(X_train.apply(lambda x:'%s %s' % (pq(x['product_title']).text(), pq(x['product_description']).text()),axis=1))
        data_data  = list(X_train.apply(lambda x:'%s' % (pq(x['query']).text()), axis = 1))

        spl_data_query = self.__get_wordlist(query_data)
        spl_data_data  = self.__get_wordlist(data_data)
        features_vec = np.zeros((len(spl_data_query),self.num_features),dtype="float32")
        
        for i in range(len(spl_data_query)):
            f1 = self.__compute_vector(spl_data_query[i])
            f2 = self.__compute_vector(spl_data_data[i])
            features_vec[i] = f1 - f2

        df = pd.DataFrame(features_vec)
        df.to_csv(filename, index = False)

    def tf_features_query_title(self, X_train):
        data = list(X_train.apply(lambda x : '%s %s' % (pq(x['query']).text() + ".", pq(x['product_title']).text()),axis=1))
        spl_data = self.__get_wordlist(data)
        spl_data = [' '.join(elem) for elem in spl_data]

        return spl_data

    def compute_features(self, X_train, type = "countMulSim", filename = "default.csv"):
        N = 50
        fitted = []
        train_query_data = list(X_train.apply(lambda x: '%s' % pq(x["query"]).text(), axis = 1))
        train_query_data = self.__get_wordlist(train_query_data)
        # Here we can compute all data using only product title
        traindata = list(X_train.apply(lambda x: '%s' % (pq(x['product_title']).text()), axis = 1))
        #traindata = list(X_train.apply(lambda x: '%s %s' % (BeautifulSoup(x['product_title']).get_text(" "), x['product_description']), axis=1))
        title_data = list(X_train.apply(lambda x: '%s' % (pq(x['product_title']).text()), axis = 1))
        description_data = list(X_train.apply(lambda x: '%s' % (pq(x['product_description']).text()), axis = 1))
        spl_traindata = self.__get_wordlist(traindata)
        title_data = self.__get_wordlist(title_data)
        description_data = self.__get_wordlist(description_data)

        global_train = []
        print(len(spl_traindata))
        print(len(title_data))
        print(len(description_data))

        for i in range(len(spl_traindata)):
            query = train_query_data[i]
            text  = dict(Counter(spl_traindata[i]))
            text_frame = pd.DataFrame(text.items(), columns=['Word', 'Count'])
            ff = pd.DataFrame(data = {'Num' : range(N), 'Importance' : [0] * N}) 
            ff2 = [0]*N
            exact = 0

            for word in query:
                vec  = [(k, elem[0], elem[1]) for k, elem in enumerate(self.model.most_similar(word, topn = N))]
                vec  = pd.DataFrame(vec, columns=['Num', 'Word', 'Importance'])
                tt = pd.merge(vec, text_frame, left_on=['Word'], right_on=['Word'])
                if type == "onlysim":
                    tt['Importance'] = tt['Importance']
                elif type == "onlycount":
                    tt['Importance'] = tt['Count']
                elif type == "countMulSim":
                    tt['Importance'] = tt['Importance'] * tt['Count']
                
                tt.drop(['Count', 'Word'], axis = 1, inplace = True)
                tt = pd.merge(ff, tt, left_on = ['Num'], right_on = ['Num'], how = 'left').fillna(0)
                tt.drop(['Importance_x'], axis = 1, inplace = True)
                ff2 = ff2 + tt['Importance_y'].values
                if word in text:
                    exact += text[word]
            index2word_set = set(self.model.index2word)
            similars_title = list(elem for elem in title_data[i] if elem in index2word_set)
            similars_title = 0 if len(similars_title) == 0 else self.model.n_similarity(query, similars_title)
            similars_desc  = list(elem for elem in description_data[i] if elem in index2word_set)
            similars_desc  = 0 if len(similars_desc) == 0 else self.model.n_similarity(query, similars_desc)

            exact_match_title  = X_train['query'][i].lower() in X_train['product_title'][i].lower()
            exact_match_desc   = False if isinstance(X_train['product_description'][i], float) else ' '.join(query) in ' '.join(description_data[i])

            phrase_match_title = False
            phrase_match_description = False
            for k in range(1,len(query)):
                phrase_match_title = phrase_match_title or ' '.join(query[0:k]) in ' '.join(title_data[i])
                phrase_match_description = phrase_match_description or \
                            False if isinstance(X_train['product_description'][i], float) else' '.join(query[0:k]) in ' '.join(description_data[i])

            c1  = Counter(query)
            c2  = Counter(title_data[i])
            c3  = Counter(description_data[i])
            cQuery = dict.fromkeys(c1 + c2 + c3, 0)
            cTitle = dict.fromkeys(c1 + c2 + c3, 0)
            cDesc  = dict.fromkeys(c1 + c2 + c3, 0)
            cQuery.update(dict(c1))
            cTitle.update(dict(c2))
            cDesc.update(dict(c3))
            cQuery  = OrderedDict(sorted(cQuery.items()))
            cTitle  = OrderedDict(sorted(cTitle.items()))
            cDesc   = OrderedDict(sorted(cDesc.items()))
            klTitle = scipy.stats.entropy(pk = cQuery.values(), qk = cTitle.values())
            klDesc  = scipy.stats.entropy(pk = cQuery.values(), qk = cDesc.values())
            klDesc  = float('inf') if math.isnan(klDesc) else klDesc
            
            ff2 = np.append(ff2, [exact, exact_match_title, exact_match_desc, similars_title, similars_desc, phrase_match_title, \
                phrase_match_description, klTitle, klDesc, len(query), len(title_data[i]), len(description_data[i])])
            global_train.append(ff2.transpose())
            
        global_train = np.array(global_train)
        names = ["Similar" + str(i) for i in range(1, 51)]
        names += ["WordMatch", "ExactMatchTitle", "ExactMatchDesc", "TitleSimilarity", "DescriptionSimilarity", "PhraseTitle", "PhraseDescription", \
                  "KLwithTitle", "KLwithDesc", "NumWordsQuery", "NumWordsTitle", "NumWordsDesc"]
        df = pd.DataFrame(global_train)
        df.columns = names
        df.to_csv(filename, index = False)        


if __name__ == '__main__':

    print("Load the training file")
    train = pd.read_csv('../Data/train.csv')
    test = pd.read_csv('../Data/test.csv')
    
    idx = test.id.values.astype(int)
    train = train.drop('id', axis=1)
    test = test.drop('id', axis=1)
    
    y = train.median_relevance.values
    train = train.drop(['median_relevance', 'relevance_variance'], axis=1)
    
    clf = WordVecApp()
    clf.create_corpus(train, test)
    clf.compute_features(test, "countMulSim", "../Features/testsimilarity5.csv")
    clf.compute_features(train, "countMulSim", "../Features/trainsimilarity5.csv")
    
    sys.exit(1)
