import pandas as pd
import numpy as np
from gensim.models import Word2Vec
from nltk.corpus import stopwords
from nltk.stem import *

import nltk

import sys
import re
import pickle
import os.path
import math
import scipy

from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from sklearn.grid_search import GridSearchCV
from sklearn.base import BaseEstimator
from sklearn.decomposition import TruncatedSVD
from sklearn.preprocessing import StandardScaler
from sklearn import decomposition, pipeline, metrics, grid_search

from pyquery import PyQuery as pq

from collections import Counter
from collections import OrderedDict

def sentence_to_wordlist(sentences, remove_stopwords=False):
        sentences = re.sub("[^a-zA-Z]"," ", sentences)
        words = sentences.lower().split()
        stop_words = ['http','www','img']

        if remove_stopwords:
            stops = set(stopwords.words("english"))
            stops.update(stop_words)
            words = [w for w in words if not w in stops]
        return words

def list_to_sentences(list_of_text, tokenizer, remove_stopwords=False):
        list_of_text = re.sub("<table>(.*?)</table>", " ", list_of_text)
        list_of_text = re.sub("[^a-zA-Z0-9\\s]","", list_of_text)
        
        if len(list_of_text) == 0:
            return [""]
        raw_sentences = tokenizer.tokenize(list_of_text.strip())
        sentences = []
        for raw_sentence in raw_sentences:
            if len(raw_sentence) > 0:
                sentences.append(sentence_to_wordlist(raw_sentence, remove_stopwords))
            elif len(raw_sentence) == 0 :
                sentences.appned("")
         
        return sentences


def get_wordlist(list_of_text):
        output = []
        #nltk.download() #to download punkt & stopwords
        tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')

        for text in list_of_text:
            output += list_to_sentences(text, tokenizer, True)
    
        return output


def compute_features(X_train, filename = "default.csv"):
        train_query_data = list(X_train.apply(lambda x: '%s' % pq(x['query']).text(), axis = 1))
        title_data = list(X_train.apply(lambda x: '%s' % (pq(x['product_title']).text()), axis = 1))
        description_data = list(X_train.apply(lambda x: '%s' % (pq(x['product_description']).text()), axis = 1))
        train_query_data = get_wordlist(train_query_data)
        title_data = get_wordlist(title_data)
        global_train = []

        for i in range(len(title_data)):
            text_query = nltk.Text(train_query_data[i])
            text_title = nltk.Text(title_data[i])
            tags_query = nltk.pos_tag(text_query)
            tags_title = nltk.pos_tag(text_title)
            counts_query = Counter(tag for word,tag in tags_query)
            counts_title = Counter(tag for word,tag in tags_title)
            
            ff2 = [counts_query['JJ'], counts_query['NN'], counts_query['VB'], counts_title['JJ'], counts_title['NN'], counts_title['VB']]
            global_train.append(ff2)

        global_train = np.array(global_train)
        names = ["AdjQuery", "NounQuery", "VerbQuery", "AdjTitle", "NounTitle", "VerbTitle"]
        df = pd.DataFrame(global_train)
        df.columns = names
        df.to_csv(filename, index = False)    

if __name__ == '__main__':

    print("Load the training file")
    train = pd.read_csv('../Data/train.csv')
    test = pd.read_csv('../Data/test.csv')
    
    idx = test.id.values.astype(int)
    train = train.drop('id', axis=1)
    test = test.drop('id', axis=1)

    compute_features(train, "../Features/POS_train.csv")
    compute_features(test, "../Features/POS_test.csv")

