import pandas as pd
import numpy as np

from gensim.models import Word2Vec
from nltk.corpus import stopwords
from nltk.stem import *

import nltk

import sys
import re
import pickle
import os.path
import math
import scipy

from bs4 import BeautifulSoup

from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from sklearn.grid_search import GridSearchCV
from sklearn.base import BaseEstimator
from sklearn.decomposition import TruncatedSVD
from sklearn.preprocessing import StandardScaler
from sklearn.feature_extraction import text
from sklearn import decomposition, pipeline, metrics, grid_search

from collections import Counter
from collections import OrderedDict

from main import *

if __name__ == '__main__':

    print("Load the training file")
    train = pd.read_csv('../Data/train.csv')
    test = pd.read_csv('../Data/test.csv')
    
    idx = test.id.values.astype(int)
    train = train.drop('id', axis=1)
    test = test.drop('id', axis=1)
    
    y = train.median_relevance.values
    train = train.drop(['median_relevance', 'relevance_variance'], axis=1)
    
    clf = WordVecApp()
    clf.create_corpus(train, test)
    print("Starting computing Features ... ")
    clf.pure_features_sum(train, "../Features/sumwordvecTrain.csv")
    print("Finished")
    clf.pure_features_sum(test,  "../Features/sumwordvecTest.csv")
    print("Finished")
    clf.pure_features_subtr(train, "../Features/minuswordvecTrain.csv")
    print("Finished")
    clf.pure_features_subtr(test,  "../Features/minuswordvecTest.csv")
    print("Finished")







