setwd("~/Workspace/kaggle/search-relevance-prediction")
df <- read.csv("Features/trainsimilarity.csv")
train <- read.csv("Data/train.csv", stringsAsFactors = FALSE)
target <- as.factor(train$median_relevance)

library(caret)
library(randomForest)
library(Metrics)
library(caTools)


spl <- sample.split(target, 0.7)

quality <- function(data, lev = NULL, model = NULL) {
  preds <- data[,"pred"]
  obs <- data[,"obs"]
  score <- ScoreQuadraticWeightedKappa(obs, preds, 1, 4)
  names(score) <- "Kappa"
  score
}

df$CountNonZero <- apply(df, 1, function(x) {sum(x[1:50]>0)})
df$SumCount <- apply(df, 1, function(x) {sum(x[1:50])})
df$Ratio <- df$SumCount/(df$CountNonZero+1)
df$nTitle <- nchar(train$product_title)
df$nDescr <- nchar(train$product_description)
df$nQuery <- nchar(train$query)
  

preprocess <- preProcess(df, method = c("center", "scale"))
df <- predict(preprocess, df)
ttrain <- subset(df, spl == TRUE)
ttest  <- subset(df, spl == FALSE)


for (i in 1:dim(test)[1]) {
	obj <- ttest[i, ]
	temp <- subset(ttrain, train$query == )
}
