setwd("~/Workspace/kaggle/search/")
df <- read.csv("Features/trainsimilarity5.csv")
train <- read.csv("Data/train.csv", stringsAsFactors = FALSE)
target <- as.factor(train$median_relevance)
folds <- read.csv("Data/Folds.csv")

index <- list()
indexOut <- list()

for (i in 1:4){
  index[[i]] <- which(folds$Folds != i)
  indexOut[[i]] <- which(folds$Folds == i)
}

library(caret)
library(randomForest)
library(Metrics)
library(plyr)
library(dplyr)

d <- train %>% group_by(query) %>% mutate(med = mean(median_relevance),
                                          sd = mean(relevance_variance), count = n())

train$MedQuery<- d$med
train$SdQuery <- d$sd
train$Count <- d$count
mer <- subset(train, select = c("query", "MedQuery", "SdQuery", "Count"))

quality <- function(data, lev = NULL, model = NULL) {
  preds <- data[,"pred"]
  obs <- data[,"obs"]
  score <- ScoreQuadraticWeightedKappa(obs, preds, 1, 4)
  names(score) <- "Kappa"
  score
}

df$CountNonZero <- apply(df, 1, function(x) {sum(x[1:50]>0.0001)})
df$SumCount <- apply(df, 1, function(x) {sum(x[1:50])})
df$Ratio <- df$SumCount/(df$CountNonZero+1)
df$nTitle <- nchar(train$product_title)
df$nDescr <- nchar(train$product_description)
df$nQuery <- nchar(train$query)
df$nRatio <- df$nQuery/df$nTitle
df$MedQuery <- train$MedQuery
df$SdQuery <- train$SdQuery
df$NumWordsDesc <- log(1+df$NumWordsDesc)
df$CountQuery <- train$Count

train.pos <- read.csv("Features//POS_train.csv")
df <- cbind(df, train.pos)

trControl <- trainControl(method = "cv", number = 4, summaryFunction = quality, verboseIter = TRUE,
                          savePredictions = TRUE, index = index, indexOut = indexOut)

tuneGrid <- expand.grid(.max_depth = c(3,5,6,8,10), .nrounds = c(150,200,250,300), .eta = c(0.3))
fit <- train(target ~ ., data = df, method = "xgbTree", trControl = trControl,
             metric = "Kappa", tuneGrid = tuneGrid, gamma = 0.3, subsample = 0.9, missing = NA)
fit
test <- read.csv("Features/testsimilarity5.csv")
test.text <- read.csv("Data/test.csv", stringsAsFactors = FALSE)

tt <- merge(test.text, mer, by='query', all.x = TRUE)
tt <- tt[!duplicated(tt), ]
tt <- tt[order(tt$id),]
rownames(tt) <- NULL

test$CountNonZero <- apply(test, 1, function(x) {sum(x[1:50]>0)})
test$SumCount <- apply(test, 1, function(x) {sum(x[1:50])})
test$Ratio <- test$SumCount/(test$CountNonZero+1)
test$nTitle <- nchar(test.text$product_title)
test$nDescr <- nchar(test.text$product_description)
test$nQuery <- nchar(test.text$query)
test$nRatio <- test$nQuery/test$nTitle
test$MedQuery <- tt$MedQuery
test$SdQuery <- tt$SdQuery
test$NumWordsDesc <- log(1+test$NumWordsDesc)
test$CountQuery <- tt$Count

test.pos <- read.csv("Features//POS_test.csv")
test <- cbind(test, test.pos)

##can be
sample2 <- read.csv("Data/sampleSubmission.csv")
sample2$prediction <- predict(fit, test)
write.csv(sample2, "xgbonsimilarityfeaturesall0001.csv", quote = FALSE, row.names = FALSE)


#Out of fold

out_of_fold <- fit$pred
b_depth <- fit$bestTune[,'max_depth']
b_nr    <- fit$bestTune[,'nrounds']
out_of_fold <- out_of_fold[out_of_fold$max_depth==b_depth & out_of_fold$nrounds == b_nr,]
write.csv(out_of_fold, "FoldsPredictions/xgbonsimilarityall0001.csv", quote = FALSE, row.names = FALSE)
